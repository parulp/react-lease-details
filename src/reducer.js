import { combineReducers } from "redux";
import widgetReducer from './container/Widget/widgetReducer';

export default combineReducers({
  widgetReducer: widgetReducer,
});