import React from 'react';
import { render} from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import Dashboard from "./pages/DashboardPage";
import reducers from './reducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(thunkMiddleware, promiseMiddleware())),
);

export default store;

const App = () => (
    <Provider store={store}>
        <Dashboard />
    </Provider>
);

render(<App />, document.getElementById("root"));
