import React from "react";
import Widget from "../container/Widget";

const DashboardPage = (props) => {
    return (
      <div>
        <div className="page-body">
          <Widget {...props} />
        </div>
      </div>
    );
};

export default DashboardPage;