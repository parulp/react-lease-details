import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import {
  getLeaseDetails,
  getAllLeaseDetails
} from './actions';
import Table from '../../component/Table';

const WidgetStyles = {
  container: {
    width: "60%",
    margin: "auto",
    border: "1px solid #ccc",
    backgroundColor: "#f3f3f3",
    marginTop: "100px",
    fontFamily: "sans-serif",
    color: "darkslategrey",
    padding: "20px"
  },
  noData: {
    padding: "10px",
    fontWeight: "800"
  }
}

class Widget extends Component {

  static propTypes = {
    getLeaseDetails: PropTypes.func,
    getAllLeaseDetails: PropTypes.func,
    leaseDetails: PropTypes.object,
    allLeaseDetails: PropTypes.instanceOf(Array),
  };

  static defaultProps = {
    getLeaseDetails: f => f,
    getAllLeaseDetails: f => f,
    leaseDetails: {},
    allLeaseDetails: [],
  }

  constructor(props) {
    super(props);
    let leaseId = "";
    /* getting Lease Id from URL if available */
    const { search } = window.location;
    if (search.indexOf("leaseId") > -1) {
      /* Handling if more than one parameter available */
      let pathParamsArr = (search.split("?")[1]).split("&");
      for (let param of pathParamsArr) {
        if (param.indexOf("leaseId") > -1) {
          leaseId = param.split("=")[1];
          break;
        }
      }
    }

    /* Setting Default State */
    this.state = {
      leaseId: leaseId && leaseId != "" ? leaseId : "",
      tabledata: [],
      allLeaseDetails: []
    };
    /* Default Header, Frequency and Days Values */
    this.headerData= ['start', 'end', 'days', 'amount'];
    this.frequencyData = { 'daily': 0, 'weekly': 6, 'fortnightly': 13, 'monthly': 27, 'yearly': 364 };
    this.daysOfWeek = { 'monday': 1, 'tuesday': 2, 'wednesday': 3, 'thursday': 4, 'friday': 5, 'saturday': 6, 'sunday': 7 };
    this.allDetailsHeaderData = ['ID', 'Tenant'];
  }

  componentWillReceiveProps(nextProps) {
    /* Calling method to construct table data once lease data received from API*/
    const { leaseDetails, allLeaseDetails } = nextProps;
    if ((leaseDetails) !== this.props.leaseDetails) {
      this.constructTableData(leaseDetails);
    }
    if ((allLeaseDetails) !== this.props.allLeaseDetails) {
      this.setState({allLeaseDetails});
    }
  }

  componentWillMount() {
    /* Fetching lease details based on lease id in the URL */
    const { getLeaseDetails, getAllLeaseDetails } = this.props;
    const { leaseId } = this.state;
    
    if (leaseId && leaseId != "" && leaseId.length > 0) {
      getLeaseDetails(leaseId);
    } else
    getAllLeaseDetails();
  }

  constructTableData = leaseData => {
    /* Using moment for Data calculations */
    let tableData = [];
    const { start_date, end_date, rent, frequency, payment_day } = leaseData;
    const end = moment(end_date);

    /* By default will set 7th day SUNDAY as payment day if given data is wrong or insufficient */
    const dayKey = payment_day.toLowerCase();
    const day = this.daysOfWeek.hasOwnProperty(dayKey) && this.daysOfWeek[dayKey] != 1 ? this.daysOfWeek[dayKey] - 1 : this.daysOfWeek.hasOwnProperty(dayKey) && this.daysOfWeek[dayKey] == 1 ? 7 : 6;

    /* By default will set payment frequency as WEEKLY if given data is wrong or insufficient */
    const freqKey = frequency.toLowerCase();
    const freq = this.frequencyData.hasOwnProperty(freqKey) ? this.frequencyData[freqKey] : 6;
    const rentPerDay = rent / (freq + 1);

    let startDate = moment(start_date);
    let start = moment(start_date);
    const startDay = startDate.isoWeekday();

    let endDate = "";
    let endDateTemp = "";
    /* Calculating the first row of loop based on day of week */
    if (startDay < day) {
      endDate = startDate.isoWeekday(day);
    } else if (startDay > day) {
      endDate = startDate.add(1, 'weeks').isoWeekday(day);
    } else if (startDay == day) {
      endDate = startDate;
    }
    let endDateDiff = end.diff(endDate, 'days');
    while (endDateDiff > 0 || endDateDiff == 0) {
      const rowData = this.constructRentAmount(start, endDate, rentPerDay);
      tableData.push(rowData);
      if(endDateDiff > 0){
      /* Calculating rows from 2nd to last of loop based on frequency */
      startDate = endDate.add(1, 'days');
      start = moment(startDate);
      endDateTemp = startDate.add(freq, 'days');
      endDate = end.diff(endDateTemp, 'days') < 0 ? end : endDateTemp;
      endDateDiff = end.diff(endDate, 'days');
      } else {
        endDateDiff = -1
      }
    }
    /* Setting state for table data */
    this.setState({ tableData });
  }

  /* To Calculate the values for each row given the start date , end date and the rent per day */
  constructRentAmount = (startDate, endDate, rentPerDay) => {
    const days = endDate.diff(startDate, 'days') + 1;
    const amount = (rentPerDay * days).toFixed(2);
    const start = startDate.format("LL");
    const end = endDate.format("LL");
    return { start, end, days, amount : "$"+amount};
  }
  
  /* To handle Lease Detail row click - Changes the URL */
  handleLeaseIdClick = (value) => window.location.href = window.location.href.split("?")[0]+"?leaseId="+value.id;

  render() {
    const { tableData, allLeaseDetails} = this.state;
    return (
      <div style={WidgetStyles.container} >
        { tableData && tableData.length>0 ? 
            (<Table tableData={tableData} headerData={this.headerData} />)
            : 
            allLeaseDetails && allLeaseDetails.length>0 ?
            (<Table tableData={allLeaseDetails} headerData={this.allDetailsHeaderData} handleRowClick={this.handleLeaseIdClick}/>)
            :
            (<span style={WidgetStyles.noData}>No Data!! Please enter leaseId in the URL in the format of “​http://127.0.0.1:8080?leaseId=12​3”</span>)
        } 
      </div>
    );
  }
}

const mapStateToProps = ({ widgetReducer }) => {
  const {
    leaseDetails,
    allLeaseDetails,
  } = widgetReducer;
  return {
    leaseDetails,
    allLeaseDetails
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getLeaseDetails,
      getAllLeaseDetails,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Widget);




