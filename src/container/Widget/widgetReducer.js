import typeToReducer from 'type-to-reducer';
import {
  LEASE_DETAILS,
  LEASE_ALL_DETAILS
} from "./actions";

const initialState = {
  leaseDetails: {},
  allLeaseDetails: [],
};

const widgetReducer = typeToReducer({
  [LEASE_DETAILS]: {
    PENDING: state => ({
      ...state,
    }),
    FULFILLED: (state, action) =>({
      ...state,
      leaseDetails: action.payload.data
    }),
    REJECTED: state => ({
        ...state,
    }),
  },
  [LEASE_ALL_DETAILS]: {
    PENDING: state => ({
      ...state,
    }),
    FULFILLED: (state, action) =>({
      ...state,
      allLeaseDetails: action.payload.data
    }),
    REJECTED: state => ({
        ...state,
    }),
  },
}, initialState);

export default widgetReducer;
