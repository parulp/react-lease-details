import axios from 'axios';

/* Uncomment the commented APIs and comments the active ones for hitting localhost APIs */

  export const getLeaseData = leaseId => () =>
  // axios.get(`http://localhost:3000/v1/leases/${leaseId}`)
  axios.get(`https://hiring-task-api.herokuapp.com/v1/leases/${leaseId}`)

  export const getAllLeaseData = () => () =>
  // axios.get(`http://localhost:3000/v1/leases`)
   axios.get(`https://hiring-task-api.herokuapp.com/v1/leases`)