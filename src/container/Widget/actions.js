/**
 * Action creators
 * These are just objects that indicates what to do but doesn"t actually perform anything
 */

import {
  getLeaseData,
  getAllLeaseData
} from './api';

export const LEASE_DETAILS = 'LEASE_DETAILS';
export const LEASE_ALL_DETAILS = 'LEASE_ALL_DETAILS';

export const getLeaseDetails = leaseId => ({
  type: LEASE_DETAILS,
  payload: getLeaseData(leaseId)
});

export const getAllLeaseDetails = () => ({
  type: LEASE_ALL_DETAILS,
  payload: getAllLeaseData()
});