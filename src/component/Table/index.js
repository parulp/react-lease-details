import React from 'react';
import PropTypes from 'prop-types';
import Row from '../Row';

const tableStyles = {
    row: {
      display: "block",
      width: "100%",
      textAlign:"center",
      border: "1px solid #ccc",
    },
    rowItem:{
      padding: "4px 0px 4px 0px",
      borderBottom: "1px dashed #ccc",
    },
    rowHeader:{
        fontWeight: "800",
        padding: "20px 0 20px 0",
        borderBottom: "1px dashed #ccc",
    }
  }

const Table = ({
    tableData, headerClass, headerData, rowClass, handleRowClick
}) => {
    return (
        <div style={tableStyles.row}>
           <div style={tableStyles.rowHeader}>
            <Row
                key="row-header"
                isHeader={true}
                data={headerData}
                columnClass={headerClass}
            />
            </div>
            {tableData.map((data, index) => (
                <div key={index} style={tableStyles.rowItem}>
                    <Row
                        key={index}
                        attributes={headerData}
                        data={data}
                        columnClass={rowClass}
                        handleRowClick={handleRowClick}
                    />
                </div>
            ))
            }
        </div>
    ); Í
}

Table.propTypes = {
    tableData: PropTypes.instanceOf(Array),
    headerData: PropTypes.instanceOf(Array),
    headerClass: PropTypes.string,
    rowClass: PropTypes.string,
};

Table.defaultProps = {
    tableData: [],
    headerData: [],
    headerClass: "",
    rowClass: "",
};

export default Table;
