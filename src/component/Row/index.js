import React from 'react';
import PropTypes from 'prop-types';

const rowStyles = {
    row: {
      display: "flex",
    },
    rowItem: {
        margin: "2px",
        width: "30%",
        borderRight: "1px dashed #ccc",
    },
    rowLeaseItem :{
        margin: "2px",
        width: "50%",
        borderRight: "1px dashed #ccc",
        cursor: 'pointer'
    }
  }

const Row = ({
    isHeader, data, columnClass, attributes, handleRowClick
}) => {
    const loopData = isHeader ? data : attributes;
    const handleClick = (value) => () => {
        if (handleRowClick) {
          handleRowClick(value);
        }
      };
    return (
        <div className={`${columnClass}`} style={rowStyles.row} onClick={handleClick(data)}>
            {loopData.map((rowdata, i) => {
                rowdata=rowdata.toLowerCase();
                return (
                <div key={i} style={loopData.length == 2? rowStyles.rowLeaseItem : rowStyles.rowItem}>{isHeader ? rowdata.toUpperCase() : data[rowdata]}</div>
                )}
            )
            }
        </div>
    ); Í
}

Row.propTypes = {
    isHeader: PropTypes.bool,
    data: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.instanceOf(Array),
    ]).isRequired,
    columnClass: PropTypes.string,
    attributes: PropTypes.instanceOf(Array)
};

Row.defaultProps = {
    isHeader: false,
    columnClass: "",
    attributes: [],
};

export default Row;